package util;
 
/**
 * 使用3线程等待方法对外部Console程序进行操作的封装功能类
 * 本类为标准输出信息获取线程类
 * 
 * @author BKMMSC 金鸡独立
 * @version 1.0
 * @since 2014
 *
 */
 
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
 
public class OutputStream extends Thread{
 
    /**
     * 从主类中得来的process对象，用于获取流
     */
    public Process process;
    /**
     * 同步文本，随时与目标程序标准输出控制台同步
     */
    public String out="";
     
    @Override
    /**
     * 线程启动
     * 
     */
    public void run(){
        BufferedReader br=new BufferedReader(new InputStreamReader(process.getInputStream()));
        try {
            printer(br);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
 
    /**
     * 更新标准错误信息
     * 
     * @param br 使用BufferedReader类型提供的标准错误流
     * @throws IOException 当读取产生错误时
     */
    public void printer(BufferedReader br) throws IOException{
        String temp;
        while((temp=br.readLine())!=null){
            out=out+temp;
        }
    }
 
    /**
     * 继承Thread类的构造方法，用于创建线程
     * 
     */
    public OutputStream() {
        super();
        // TODO 自动生成的构造函数存根
    }
 
    /**
     * 继承Thread类的构造方法，用于创建线程并给与名称
     * 
     */
    public OutputStream(String name) {
        super(name);
        // TODO 自动生成的构造函数存根
    }
 
     
}