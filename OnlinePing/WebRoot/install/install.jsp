<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>安装和初始化完成|金鸡独立在线Ping系统</title>
<%
	String os=request.getParameter("os");
	String url=request.getParameter("url");
	String username=request.getParameter("username");
	String password=request.getParameter("password");
	application.setAttribute("os", os);
	application.setAttribute("dburl", url);
	application.setAttribute("username", username);
	application.setAttribute("password", password);
	int pawdlength=password.length();
	String pawdhid="";
	for(int i=0;i<pawdlength;++i){
		pawdhid+="*";
	}
%>
</head>
<body>
<center>
<h1><font color="red">安装和初始化完成|金鸡独立在线Ping系统</font></h1>
已经获得以下信息：<br/>
os=<%=application.getAttribute("os") %><br/>
dburl=<%=application.getAttribute("dburl") %><br/>
username=<%=application.getAttribute("username") %><br/>
password=<%=pawdhid %><br/>
已经设置到application，password是被隐藏的，无需担心。<br/>
您现在需要执行根目录下的database.sql文件到数据库来完成最后的安装工作。
</center>
</body>
</html>