<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Linux服务器调试初始化</title>
<% 
	String os="linux";
	String url="jdbc:mysql://localhost:3306/onlineping";
	String username="root";
	String password="123456";
	application.setAttribute("os", os);
	application.setAttribute("dburl", url);
	application.setAttribute("username", username);
	application.setAttribute("password", password);
	int pawdlength=password.length();
	String pawdhid="";
	for(int i=0;i<pawdlength;++i){
		pawdhid+="*";
	}
%>
</head>
<body>
<font color="red">调试人员专用，如果你是服务器部署者请关闭本界面并重新使用/install/index.jsp进行初始化！</font><br/>
<br/>
log-
application things:<br/>
<br/>
jdbc-connection-url=<%=application.getAttribute("dburl") %>;<br/>
jdbc-connection-username=<%=application.getAttribute("username") %>;<br/>
jdbc-connection-password=!hidden!-<%=pawdhid %>;<br/>
system-operator-system-version=linux;<br/>
<br/>
another-error:null;
<br/>
<br/>
@end!
</body>
</html>