<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>设置任务|即时任务 powered by 金鸡独立</title>
<%
	session.removeAttribute("tdthread");
%>
</head>
<body>
<center>
<h1><font color="red">设置任务|金鸡独立在线Ping子系统——金鸡独立即时Ping</font></h1>
欢迎使用金鸡独立在线ping的子系统——金鸡独立即时ping！<br/>
金鸡独立即时ping系统是一种单服务器、单线程的ping系统，即您发出请求之后立刻执行，立刻打开命令行监视，只有本服务器的单个线程进行小范围的攻击的系统。
<br/>
<font color="red">
本系统一般适用于正式攻击之前进行小范围探测的系统，所以设置的参数数量级不宜过大！
</font>
<br/>
<form action="timelydoing.jsp" method="post">
<table border="1">
	<tr>
		<td>目标地址：</td>
		<td><input type="text" id="address" name="address" style ="width:400px;"></td>
	</tr>
	<tr>
		<td>攻击次数：</td>
		<td><input type="text" id="number" name="number" style ="width:400px;"></td>
	</tr>
	<tr>
		<td>攻击频率（0为连续攻击）：</td>
		<td><input type="text" id="rate" name="rate" style ="width:400px;"></td>
	</tr>
	<tr>
		<td><input type="submit" value="提交并执行" /></td>
		<td><input type="reset" value="重新填写表单" /></td>
	</tr>
</table>
</form>
</center>
</body>
</html>