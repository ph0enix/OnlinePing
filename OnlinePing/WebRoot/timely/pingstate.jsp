<%@page import="util.ExternalProgramUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%if(session.getAttribute("isstop").equals("stoped")==false){ %>
<meta http-equiv="refresh" content="1" >
<%} %>
<title>pingstate</title>
<%
	String content=null;
	ExternalProgramUtil epu=null;
	try{
		epu=(ExternalProgramUtil) session.getAttribute("epu");
		content=epu.getOutputinfo();
	}catch(NullPointerException npe){
		npe.printStackTrace();
		out.println("<font color=\"red\">出现严重错误：无法得到pingstate-epu，请联系QQ2276768747来解决问题！</font>");
	}
%>
</head>
<body>
<%if(session.getAttribute("isstop").equals("stoped")==false){ %>
<a href="stopfresh.jsp">停止页面刷新</a><br/>
<%}else{ %>
<a href="startfresh.jsp">开始页面刷新</a><br/>
<%} %>
<%if(epu.getErrorinfo().equals("")==false){ %>
<font color="red">ping程序出现错误，请杀死ping并且联系QQ2276768747，如果你知道错误的含义可以查看以下信息：</font><br/>
<%=epu.getErrorinfo() %>
<%} %>
<%=content %>
</body>
</html>