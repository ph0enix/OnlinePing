<%@page import="java.io.IOException"%>
<%@page import="template.TimelyDoingTemplate"%>
<%@page import="util.ExternalProgramUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>执行任务|即时任务 powered by 金鸡独立</title>
<%
	session.setAttribute("isstop", "nostoped");
	TimelyDoingTemplate tdt=new TimelyDoingTemplate();
	tdt.address=request.getParameter("address");
	tdt.number=Integer.parseInt(request.getParameter("number"));
	tdt.rate=Integer.parseInt(request.getParameter("rate"));
	
	ExternalProgramUtil epu=null;
	if(application.getAttribute("os").equals("linux")){
		try {
			if(tdt.rate!=0){
				epu=new ExternalProgramUtil("ping -c "+tdt.number+" -i "+tdt.rate+" "+tdt.address);
			}else{
				epu=new ExternalProgramUtil("ping -c "+tdt.number+" "+tdt.address);
			}
		} catch (IOException e) {
			// TODO 自动生成的 catch 块
			System.out.println("错误，发生在PingThread类的构造方法中的地一个try语句块，详细信息：\n"+e);
		}
	}else if(application.getAttribute("os").equals("windows")){
		try {
			if(tdt.rate==0){
				epu=new ExternalProgramUtil("ping -n "+tdt.number+" "+tdt.address);
			}else{
%>
<script type="text/javascript">
alert("Exception-00000:timeltdoing.jsp near line at 36;host OS is Windows!so you can not use tdt.rate,plaese give it a zero!");
</script>
<%				
			}
		} catch (IOException e) {
			// TODO 自动生成的 catch 块
			System.out.println("错误，发生在PingThread类的构造方法中的地一个try语句块，详细信息：\n"+e);
		}
	}else{
%>
<script type="text/javascript">
alert("Error-00002:timeltdoing.jsp near line at 37;os is null!NullPointerException!");
</script>
<%
	}
	
	session.setAttribute("epu", epu);
	
%>
</head>
<body>
<center>
<h1><font color="red">设置任务|金鸡独立在线Ping子系统——金鸡独立即时Ping</font></h1>
<br/>
特别注意：你可以选择下面的停止页面刷新和开始页面刷新来复制和查看log，但是请不要随意关闭此界面，否则您不但在也无法查看log，而且有可能对session造成损坏。
<br/>
如果需要停止并在也不需要查看log和ping进程，请点击：
<a href="killit.jsp">杀死Ping</a>
来安全地关闭ping进程和log刷新。
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<iframe scrolling="auto" height="500" width="1000" src="pingstate.jsp"></iframe>
</center>
</body>
</html>